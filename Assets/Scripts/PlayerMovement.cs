﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    private float controlDirH = 0;
    [SerializeField] float velocidad = 1;
    [SerializeField] float jumpForce = 5;
    Rigidbody2D myRgb2D;
    Animator myAnimator;
    private bool isGrounded = true;
    
    // Start is called before the first frame update
    void Start()
    {
        myRgb2D = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //Controlar parametros de animaciones (animaciones)
        myAnimator.SetBool("IsGrounded", isGrounded);
        myAnimator.SetFloat("Speed", Mathf.Abs(myRgb2D.velocity.x));
        
        //Controlar dirección hacia donde ve el personaje (movimiento)
        if (Math.Abs(controlDirH) > Mathf.Epsilon)
        {
            transform.localScale = new Vector3(Mathf.Sign(controlDirH) * -1, 1, 1);
        }
    }

    private void FixedUpdate()
    {
        //asignar velocidad al personaje (movimiento)
        myRgb2D.velocity = new Vector2(velocidad * controlDirH, myRgb2D.velocity.y);
    }

    //Detección de contacto con el piso
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Platforms"))
        {
            isGrounded = true;
        }
    }

    //Detección de salida del contacto con el piso
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Platforms"))
        {
            isGrounded = false;
        }
    }

    //Reconocer Input del salto (input)
    public void Jump(InputAction.CallbackContext context)
    {
        if (isGrounded && context.performed)
        {
            myRgb2D.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }
    }

    //Reconocer Input del movimiento (input)
    public void Movement(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            controlDirH = context.ReadValue<Vector2>().x;
        }
    }
}