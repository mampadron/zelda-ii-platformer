﻿// Este script controla el movimiento y las físicas del jugador dentro del juego
using UnityEngine;

public class MovimientoJugador : MonoBehaviour
{
    public bool drawDebugRaycasts = true;	//Se deben visualizar los chequeos Ambientales?

    [Header("Movement Properties")]
    #region Propiedades del Movimiento
    public float velocidad = 8f;			//Velocidad del jugador
    public float coyoteDuration = .05f;		//Cuanto tiempo despues de caer puede saltar el jugador
    public float maxFallSpeed = -25f;		//Max speed player can fall
    #endregion
    
    [Header("Jump Properties")]
    #region Propiedades del Salto
    public float jumpForce = 5f;			//Fuerza inicial del salto
    public float jumpHoldForce = .25f;		//Fuerza incremental cuando se mantiene presionado el salto
    public float jumpHoldDuration = .2f;	//Por cuanto tiempo se puede mantener presionado el salto
    #endregion
    
    [Header("Environment Check Properties")]
    #region Propiedades de Chequeo Ambiental
    public float footXOffset = .3f;			//X Offset del raycast de los pies
    public float footYOffset = .1f;			//Y Offset del raycast de los pies
    public float groundDistance = .2f;		//Distancia a la que se considera que el jugador está en el suelo
    public LayerMask groundLayer;			//Capa del suelo
    #endregion

    [Header ("Status Flags")]
    #region Inicadores de Status
    public bool isOnGround;					//Está el jugador en el suelo?
    public bool isJumping;					//Está el jugador saltando?
    #endregion
    
    private PlayerInput _input;				//El input actual del jugador
    private BoxCollider2D _bodyCollider;	//El componente collider
    private Rigidbody2D _myRgb2D;			//El componente rigidbody

    #region Propiedades Adicionales
    private float _jumpTime;				//Variable to hold jump duration
    private float _coyoteTime;				//Variable to hold coyote duration
    
    private float _originalXScale;			//Escala original en el eje X
    private int _direction = -1;			//Dirección a la cual está viendo el jugador
    #endregion
    
    // Start is called before the first frame update
    void Start()
    {
        //Obtener referencia a los componentes requeridos
        _input = GetComponent<PlayerInput>();
        _myRgb2D = GetComponent<Rigidbody2D>();
        _bodyCollider = GetComponent<BoxCollider2D>();

        //Almacenar la escala original en el eje x del jugador
        _originalXScale = transform.localScale.x;
    }

    private void FixedUpdate()
    {
	    //Chequear el ambiente para determinar estatus
	    PhysicsCheck();

	    //Procesar movimientos en tierra y aire
	    GroundMovement();		
	    MidAirMovement();
    }

	void PhysicsCheck()
	{
		//Se inicia asumiendo que el jugador no está en el suelo
		isOnGround = false;

		//RayCasts para el pie izquierdo y derecho
		RaycastHit2D leftCheck = Raycast(new Vector2(-footXOffset, footYOffset), Vector2.down, groundDistance);
		RaycastHit2D rightCheck = Raycast(new Vector2(footXOffset, footYOffset), Vector2.down, groundDistance);

		//Si cualquiera de los dos rays toca el suelo, el jugador está en el suelo
		if (leftCheck || rightCheck)
			isOnGround = true;
	}

	void GroundMovement()
	{
		//Calcular la velocidad horizontal deseada basada en los inputs
		float xVelocity = velocidad * _input.horizontal;

		//Si el signo de la velocidad horizontal y la dirección no son iguales, voltear al personaje
		if (xVelocity * _direction < 0f)
			InvertirDireccion();

		//Aplicar la velocidad deseada 
		_myRgb2D.velocity = new Vector2(xVelocity, _myRgb2D.velocity.y);

		//Si el jugador está en el suelo, extender la ventana del coyote time
		if (isOnGround)
			_coyoteTime = Time.time + coyoteDuration;
	}

	void MidAirMovement()
	{
		//Si se presiona el botón jump Y el jugaor no se encuentra ya saltando Y YA SEA
		//que el jugador este en el suelo o dentro de la ventana de tiempo del coyote time...
		if (_input.jumpPressed && !isJumping && (isOnGround || _coyoteTime > Time.time))
		{
			//...El jugador ya no se encuentra en el suelo y está saltando...
			isOnGround = false;
			isJumping = true;

			//...registrar el tiempo en el que el jugador dejará de poder incrementar la fuerza el salto...
			_jumpTime = Time.time + jumpHoldDuration;

			//...agregar la fuerza del salto al rigidbody...
			_myRgb2D.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);

			//...aquí se debería ejecutar el sonido del salto
			//ejecutar sonido Salto
		}
		//de lo contrario, si se encuentra actualmente dentro de la ventana del salto...
		else if (isJumping)
		{
			//...y el boton de salto está sieno presionado, aplicar fuerza incremental al rigidbody...
			if (_input.jumpHeld)
				_myRgb2D.AddForce(new Vector2(0f, jumpHoldForce), ForceMode2D.Impulse);

			//...y si el tiempo de salto se acabó, colocar el flag isJumping en false
			if (_jumpTime <= Time.time)
				isJumping = false;
		}

		//If player is falling to fast, reduce the Y velocity to the max
		if (_myRgb2D.velocity.y < maxFallSpeed)
			_myRgb2D.velocity = new Vector2(_myRgb2D.velocity.x, maxFallSpeed);
	}

	void InvertirDireccion()
	{
		//Voltear al personaje invirtiendo la dirección
		_direction *= -1;
		//Aplicar la nueva escala
		transform.localScale = new Vector3(_originalXScale * _direction * -1, 1, 1);
	}

	#region Metodos Raycast para detectar colisión con el GrounLayer
	//These two Raycast methods wrap the Physics2D.Raycast() and provide some extra
	//functionality
	private RaycastHit2D Raycast(Vector2 offset, Vector2 rayDirection, float length)
	{
		//Call the overloaded Raycast() method using the ground layermask and return 
		//the results
		return Raycast(offset, rayDirection, length, groundLayer);
	}

	private RaycastHit2D Raycast(Vector2 offset, Vector2 rayDirection, float length, LayerMask mask)
	{
		//Record the player's position
		Vector2 pos = transform.position;

		//Send out the desired raycast and record the result
		RaycastHit2D hit = Physics2D.Raycast(pos + offset, rayDirection, length, mask);

		//If we want to show debug raycasts in the scene...
		if (drawDebugRaycasts)
		{
			//...determine the color based on if the raycast hit...
			Color color = hit ? Color.red : Color.green;
			//...and draw the ray in the scene view
			Debug.DrawRay(pos + offset, rayDirection * length, color);
		}

		//Return the results of the raycast
		return hit;
	}
	#endregion
}
