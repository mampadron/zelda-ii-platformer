﻿// Este Script controla los Inputs del Jugador. Tiene 2 propósitos principales: 1) separar
// la detección de los inputs y 2) mantener los inputs del Update() sincronizaos con FixedUpdate()

using UnityEngine;
using UnityEngine.InputSystem;

//Nos aseguramos primero de que éste script se ejecute antes que cualquier otro script del jugador
//para prevenir demoras/retrasos en los input
[DefaultExecutionOrder(-100)]
public class PlayerInput : MonoBehaviour
{
    
    [HideInInspector] public float horizontal = 0;         //Float que almacena el input horizontal
    [HideInInspector] public bool jumpHeld;                //Bool que almacena si se mantiene presionado jump 
    [HideInInspector] public bool jumpPressed;             //Bool que almacena si se presionó jump    
    private bool _readyToClear;                            //Bool que se utiliza para mantener sincronizados los input

    private float _controlDirH = 0f;
    private bool _controlJumpPressed = false;
    private bool _controlJumpHeld = false;
    
    private void Update()
    {
        //Limpiar todos los valores de Inputs Actuales
        ClearInput();

        #region Si queremos detener el reconocimiento de Inputs
        //Si ocurre algo que queramos identificar para no procesar los controles, salimos
        /*
        if (GameManager.IsGameOver() || GameManager.IsGamePaused())
            return;
        */
        #endregion

        //Procesar los inputs
        ProcessInputs();

        //Restringir(Clamp) el input horizontal a estar entre -1 y 1
        horizontal = Mathf.Clamp(horizontal, -1f, 1f); 
    }

    void FixedUpdate()
    {
        //En FixedUpdate() se establece un flag que permite limpiar los inputs durante 
        //el siguiente Update(). Esto asegura que el código utilice los inputs actuales
        _readyToClear = true;
    }
    
    private void ClearInput()
    {
        //Si aún no estamos listos para limpiar los input, salimos
        if (!_readyToClear)
            return;

        //Resetear todos los inputs
        horizontal		= 0f;
        jumpPressed		= false;
        jumpHeld		= false;

        _readyToClear	= false;
    }

    private void ProcessInputs()
    {
        //Acumular input horizontal
        horizontal		+= _controlDirH;

        //Acumular input de botones
        jumpPressed = jumpPressed || _controlJumpPressed;
        jumpHeld	= jumpHeld || _controlJumpHeld;
        
        //Debido a que _controlJumpPressed solo debe activarse en el frame en que se hace el salto,
        //entonces lo desactivamos para el siguiente frame
        if (_controlJumpPressed)
            _controlJumpPressed = false;

    }
    
    //Reconocer Input del salto (input)
    public void Jump(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            _controlJumpPressed = true;
        }
        if (context.started)
        {
            _controlJumpHeld = true;
        }
        if (context.canceled)
        {
            _controlJumpHeld = false;
        }

    }

    //Reconocer Input del movimiento (input)
    public void Movement(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            _controlDirH = context.ReadValue<Vector2>().x;
        }
    }
}
