﻿// Este script controla las animaciones del personaje Jugador. Normalmente, la mayoría
// de las veces esta funcionalidad se agregaría al script PlayerMovement en lugar de tener
// su propio script, ya que sería más eficiente. Sin embargo, para efectos de aprendizaje,
// se ha separado esta funcionalidad.

using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
    MovimientoJugador movement;	//Referencia al componente script PlayerMovement
    Rigidbody2D rigidBody;		//Referencia al componente Rigidbody2D
    Animator anim;				//Referencia al componente Animator

    int groundParamID;			//ID del parametro isOnGround
    int speedParamID;			//ID of the speed parameter
    
    void Start()
    {
        //Obtener el int de los parámetros del Animator. Esto es mucho más eficiente
        //que estar pasando strings al Animator
        groundParamID = Animator.StringToHash("IsGrounded");
        speedParamID = Animator.StringToHash("Speed");

        //Obtener referencias a los componentes necesarios
        movement	= GetComponent<MovimientoJugador>();
        rigidBody	= GetComponent<Rigidbody2D>();
        anim		= GetComponent<Animator>();
		
        //Si alguno de los componentes no existe...
        if(movement == null || rigidBody == null || anim == null)
        {
            //...mostrar error y entonces remover éste componente
            Debug.LogError("A needed component is missing from the player");
            Destroy(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Update the Animator with the appropriate values
        anim.SetBool(groundParamID, movement.isOnGround);

        //Use the absolute value of speed so that we only pass in positive numbers
        anim.SetFloat(speedParamID, Mathf.Abs(rigidBody.velocity.x));
    }
}
